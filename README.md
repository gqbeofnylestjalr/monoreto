# Monoreto

![IMAGE](./doc/promo.gif)

Monoreto is a social network with monetization of likes. This site is written using Bootstrap, PHP, jQuery

## Requirements

You will need the following things properly installed on your computer:

* [Git](https://git-scm.com/)
* [XAMPP](https://www.apachefriends.org/ru/index.html) (development)
* [Apache](https://httpd.apache.org/) (production)

## Installation

* `git clone <repository-url>`
* move project folder to apache folder "/htdocs"

## Running / Development

* start xampp server `sudo /opt/lampp/manager-linux-x64.run`
* start apache web server
Visit your app at [http://localhost:80](http://localhost:80). Project files are in folder `~/htdocs`

### Production

* upload files to a folder on the server, for example: `srv/www/monoreto`
* edit the apache configuration, the file on the server for example: `/etc/lighttpd/lighttpd.conf`
* start apache server, command for example: `/etc/init.d/apache2 start`

## Further Reading / Useful Links

* [Apache](https://httpd.apache.org/)
* [Bootstrap](https://getbootstrap.com/)
* [jQuery](https://jquery.com/)
* [PHP](https://www.php.net/)
